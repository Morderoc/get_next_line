/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_bzero.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: stherman <stherman@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/20 14:54:32 by stherman          #+#    #+#             */
/*   Updated: 2013/11/22 09:08:36 by stherman         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include	<string.h>

void		ft_bzero(void *s, size_t n)
{
	unsigned char	*tmp;

	if (n == 0)
		return ;
	tmp = (unsigned char *)(s);
	while (--n > 0)
		tmp[n] = 0;
	tmp[n] = 0;
}
