/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnew.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: stherman <stherman@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/20 16:43:55 by stherman          #+#    #+#             */
/*   Updated: 2013/12/05 10:35:53 by stherman         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include	<stdlib.h>
#include	<string.h>
#include	"libft.h"

char		*ft_strnew(size_t size)
{
	void	*tmp;

	if ((tmp = xmalloc(size + 1)) == NULL || size == 0)
		return (NULL);
	ft_bzero(tmp, size + 1);
	return ((char *)(tmp));
}
