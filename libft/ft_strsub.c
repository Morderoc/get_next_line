/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsub.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: stherman <stherman@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/20 18:36:50 by stherman          #+#    #+#             */
/*   Updated: 2013/12/05 10:37:45 by stherman         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include	<stdlib.h>
#include	<string.h>
#include	"libft.h"

char		*ft_strsub(const char *s, unsigned int start, size_t len)
{
	char			*tmp;
	unsigned int	end;
	register int	i;

	i = 0;
	end = start + len;
	if (s == NULL || (tmp = (char *)(xmalloc(sizeof(char) * len + 1))) == NULL)
		return (NULL);
	while (start < end)
		tmp[i++] = s[start++];
	tmp[i] = '\0';
	return (tmp);
}
