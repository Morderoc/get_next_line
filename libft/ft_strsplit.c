/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsplit.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: stherman <stherman@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/21 15:32:40 by stherman          #+#    #+#             */
/*   Updated: 2013/12/05 10:37:16 by stherman         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include	<stdlib.h>
#include	"libft.h"

static int		ft_count_word(char *s, char c)
{
	int		i;
	int		cpt;

	i = 0;
	cpt = 0;
	while (s[i])
	{
		if (s[i] != c)
		{
			cpt = cpt + 1;
			while (s[i] && s[i] != c)
				i = i + 1;
			while (s[i] && s[i] == c)
				i = i + 1;
		}
		i = i + 1;
	}
	return (cpt);
}

static int		ft_count_char(char *s, char c, int *i)
{
	int		cpt;

	cpt = 0;
	while (s[*i] && s[*i] != c)
	{
		cpt = cpt + 1;
		*i = *i + 1;
	}
	return (cpt);
}

char			**ft_strsplit(const char *s, char c)
{
	int   i;
	int   n;
	int   word;
	char  **tab;

	i = 0;
	n = 0;
	if (s == NULL)
		return (NULL);
	word = ft_count_word(ft_strdup((char *)(s)), c);
	tab = (char **)(xmalloc(sizeof(char *) * word));
	while (s[i] && word > 0)
	{
		if (s[i] != c)
		{
			tab[n] = ft_strdup((char *)(s) + i);
			tab[n][ft_count_char((char *)(s), c, &i)] = '\0';
			n = n + 1;
			word = word - 1;
		}
		i = i + 1;
	}
	tab[ft_count_word(ft_strdup((char *)(s)), c) - 1] = NULL;
	return (tab);
}
