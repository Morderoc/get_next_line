/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   memory.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: stherman <stephane.herman@live.fr>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/09/03 14:32:27 by stherman          #+#    #+#             */
/*   Updated: 2013/12/05 10:00:45 by stherman         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include		<stdlib.h>
#include		"libft.h"

char			*ft_realloc(char *str, int size)
{
	char		*new;

	if (size == 0)
		return (str);
	if ((new = (char *)xmalloc((ft_strlen(str) + size + 1)
							   * sizeof(*str))) == NULL)
		return (NULL);
	*new = '\0';
	ft_strcat(new, str);
	*str = '\0';
	xfree(str);
	return (new);
}
