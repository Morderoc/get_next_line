/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strstr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: stherman <stherman@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/20 14:59:56 by stherman          #+#    #+#             */
/*   Updated: 2013/11/21 04:29:45 by stherman         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include	<string.h>
#include	<stdlib.h>

char		*ft_strnstr(const char *s1, const char *s2, size_t n)
{
	register unsigned int	i;
	register unsigned int	j;

	i = 0;
	j = 0;
	if (*s1 && !*s2)
		return ((char *)(s1));
	while (s1[i] && i < n)
	{
		if (s1[i] == s2[j])
			j = j + 1;
		else
			j = 0;
		if (!s2[j])
		{
			i = i - (j - 1);
			return ((char *)(s1 + i));
		}
		i = i + 1;
	}
	return (NULL);
}
