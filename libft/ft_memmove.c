/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memmove.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: stherman <stherman@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/20 14:55:31 by stherman          #+#    #+#             */
/*   Updated: 2013/11/28 13:48:44 by stherman         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include	<string.h>
#include	"libft.h"

void		*ft_memmove(void *dest, const void *src, size_t n)
{
	void	*sav;
	char	*c;

	c	= (char *)(src);
	sav	= dest;
	if ((src < dest) && (size_t)((char *)(dest) - (char *)(src)) < n)
	{
		c = (char *)(src) + n - 1;
		while (c >= (char *)(src))
		{
			*(c + ((char *)(dest) - (char *)(src))) = *c;
			c--;
		}
	}
	else
		ft_memcpy(dest, src, n);
	return (sav);
}
