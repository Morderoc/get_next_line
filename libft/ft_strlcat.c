/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlcat.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: stherman <stherman@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/20 14:58:21 by stherman          #+#    #+#             */
/*   Updated: 2013/11/28 13:56:10 by stherman         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include	<string.h>
#include	"libft.h"

size_t		ft_strlcat(char *dst, const char *src, size_t size)
{
	register char		*d;
	register const char	*s;
	register size_t		n;
	size_t				dlen;

	d		= dst;
	s		= src;
	n		= size;
	while (*d != '\0' && n-- != 0)
		d++;
	dlen	= d - dst;
	n		= size - dlen;
	if (n == 0)
		return (dlen + ft_strlen(s));
	while (*s != '\0')
	{
		if (n != 1)
		{
			*d++ = *s;
			n--;
		}
		s++;
	}
	*d = '\0';
	return (dlen + (s - src));
}
