/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memccpy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: stherman <stherman@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/20 14:55:07 by stherman          #+#    #+#             */
/*   Updated: 2013/11/28 13:46:27 by stherman         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include	<stdlib.h>
#include	<string.h>

void		*ft_memccpy(void *dest, const void *src, int c, size_t n)
{
	unsigned char			*d;
	unsigned char			*s;
	register unsigned int	i;

	d = (unsigned char *)(dest);
	s = (unsigned char *)(src);
	i = 0;
	c &= 0xff;
	while (i < n)
	{
		if ((*d++ = *s++) == c)
			return (d);
		i++;
	}
	return (NULL);
}
