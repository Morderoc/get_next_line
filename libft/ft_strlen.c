/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlen.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: stherman <stherman@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/20 14:56:41 by stherman          #+#    #+#             */
/*   Updated: 2013/11/21 08:53:22 by stherman         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include	<string.h>

size_t		ft_strlen(const char *str)
{
	register unsigned int	size;

	size = 0;
	while (*str)
	{
		str++;
		size++;
	}
	return (size);
}
