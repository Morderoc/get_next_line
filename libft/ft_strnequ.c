/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnequ.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: stherman <stherman@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/20 18:24:57 by stherman          #+#    #+#             */
/*   Updated: 2013/11/25 11:13:52 by stherman         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include	<stdlib.h>
#include	<string.h>

int			ft_strnequ(const char *s1, const char *s2, size_t n)
{
	register unsigned int	i;

	i = 0;
	if (s1 == NULL || s2 == NULL)
		return (-1);
	while (i < n && s1[i] && s2[i] && s1[i] == s2[i])
		i++;
	if (i > 0)
		i--;
	return (s1[i] == s2[i]);
}
